import React from "react";
import twitterIcon from "../image/i-twitter.png";
import facebookIcon from "../image/i-facebook.png";
import linkedinIcon from "../image/i-linkedin.png";
import emailIcon from "../image/i-email.png";

export default function Footer() {
  return (
    <footer className="c-footer">
      <div className="c-footer__mobile">
        <div className="c-footer__links">
          <div className="c-footer__link">Privacy Policy</div>
          <div className="c-footer__link">Terms of Use</div>
        </div>
        <div className="c-mobile__link">
          <div className="c-mobile__title">Site Links</div>
          <ul>
            <li>Home</li>
            <li>About Us</li>
            <li>Insights</li>
            <li>Events</li>
            <li>Contact Us</li>
          </ul>
        </div>
      </div>
      <div className="c-footer__desktop">
        <div className="c-footer__left">
          <span>Call us at 111-222-3333 for more information</span>
        </div>
        <div className="c-footer__right">
          <div className="c-footer__icons">
            <span>Social Share</span>
            <div className="c-footer__icon">
              <img src={twitterIcon} alt="" />
            </div>
            <div className="c-footer__icon">
              <img src={facebookIcon} alt="" />
            </div>
            <div className="c-footer__icon">
              <img src={linkedinIcon} alt="" />
            </div>
            <div className="c-footer__icon">
              <img src={emailIcon} alt="" />
            </div>
          </div>
        </div>
      </div>
    </footer>
  );
}
