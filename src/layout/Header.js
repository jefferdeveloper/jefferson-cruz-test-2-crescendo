import React, { useState } from "react";
import logoMobile from "../image/logo-mobile.png";
import logoMobileActive from "../image/logo-mobile_active.png";
import menuIcon from "../image/i-menu.png";
import closeIcon from "../image/i-close.png";
import logo from "../image/logo.png";
import logoLabel from "../image/logo-label.png";

export default function Header() {
  const [mobileMenu, _mobileMenu] = useState(false);

  const mobileMenuToggle = () => {
    _mobileMenu(!mobileMenu);
  };

  return (
    <header>
      <div className="c-mobile">
        <div className="c-mobile__header">
          <div className="c-mobile__logo">
            <img src={logoMobile} alt="" />
          </div>
          <div className="c-mobile__menu" onClick={mobileMenuToggle}>
            <img src={menuIcon} alt="" />
          </div>
        </div>
        <div
          className={
            mobileMenu ? "c-mobile__nav c-mobile__nav--active" : "c-mobile__nav"
          }
        >
          <div className="c-mobile__mask"></div>
          <div className="c-mobile__content">
            <div className="c-mobile__upper">
              <div className="c-mobile__logoActive">
                <img src={logoMobileActive} alt="" />
              </div>
              <div className="c-mobile__close" onClick={mobileMenuToggle}>
                <img src={closeIcon} alt="" />
              </div>
            </div>
            <div className="c-mobile__lower">
              <div className="c-mobile__link">
                <div className="c-mobile__title">Site Links</div>
                <ul>
                  <li>Home</li>
                  <li>About Us</li>
                  <li>Insights</li>
                  <li>Events</li>
                  <li>Contact Us</li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div className="c-header">
        <div className="c-header__left">
          <div className="c-header__logo">
            <img src={logo} alt="" />
          </div>
          <div className="c-header__logoLabel">
            <img src={logoLabel} alt="" />
          </div>
        </div>
        <div className="c-header__right">
          <ul>
            <li>Home</li>
            <li>About Us</li>
            <li>Insights</li>
            <li>Events</li>
            <li>Contact Us</li>
          </ul>
        </div>
      </div>
    </header>
  );
}
