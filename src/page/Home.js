import React from "react";
import cardImg1 from "../image/insight-1.png";
import cardImg2 from "../image/insight-2.png";
import cardImg3 from "../image/insight-3.png";
import logoFooter from "../image/logo-footer.png";
import Slider from "react-slick";

export default function Home() {
  return (
    <div className="c-home">
      <div className="c-banner">
        <p className="c-banner__label">Research Professional Platform</p>
        <div className="c-banner__wrap">
          <p className="c-banner__heading">ACME Wealth</p>
          <h3>Management Platforms</h3>
          <div className="c-banner__divider"></div>
          <div className="c-banner__list">
            <p>Investment excellence.</p>
            <p>persity of thought.</p>
            <p>Organizational strength.</p>
          </div>
        </div>
      </div>
      <div className="c-insight">
        <div className="c-insight__wrap">
          <div className="c-insight__info">
            <h4>ACME Insights</h4>
            <h5>How are factors being used around the world?</h5>
          </div>
          <Slider
            className="c-insight__cards"
            dots={true}
            infinite={false}
            speed={500}
            slidesToShow={3}
            slidesToScroll={1}
            arrows={false}
            responsive={[
              {
                breakpoint: 50000,
                settings: "unslick",
              },
              {
                breakpoint: 700,
                settings: {
                  slidesToShow: 1,
                },
              },
            ]}
          >
            <div className="c-insight__card">
              <div className="c-insight__img">
                <img src={cardImg1} alt="" />
              </div>
              <div className="c-insight__title">
                Global Factor Investing Study
              </div>
            </div>
            <div className="c-insight__card c-insight__card--2">
              <div className="c-insight__img">
                <img src={cardImg2} alt="" />
              </div>
              <div className="c-insight__title">2019 Outlook</div>
            </div>
            <div className="c-insight__card c-insight__card--3">
              <div className="c-insight__img">
                <img src={cardImg3} alt="" />
              </div>
              <div className="c-insight__title">Capital Market Assumptions</div>
            </div>
          </Slider>
        </div>
      </div>
      <div className="c-motto">
        <div className="c-motto__wrap">
          <h2>Our Commitment to Professionals</h2>
          <p>
            We help our partners deliver industry leading results with a
            commitment to excellence, thought-provoking insights and experienced
            distribution. We are laser focused on our shared goal – helping
            clients achieve their objectives. 
          </p>
          <div className="o-btn__wrap">
            <div className="o-button">Contact Us</div>
          </div>
        </div>
      </div>
      <div className="c-event">
        <div className="c-event__wrap">
          <div className="c-event__info">
            <h4>
              Upcoming <span>Events</span>
            </h4>
            <h5>This needs a great tagline, but I’ll fill it in later</h5>
          </div>
          <Slider
            className="c-event__cards"
            dots={true}
            infinite={false}
            speed={500}
            slidesToShow={3}
            slidesToScroll={1}
            arrows={false}
            responsive={[
              {
                breakpoint: 50000,
                settings: "unslick",
              },
              {
                breakpoint: 700,
                settings: {
                  slidesToShow: 1,
                },
              },
            ]}
          >
            <div className="c-event__card">
              <div className="c-event__date">
                <div className="c-event__dateWrap">
                  <div className="c-event__month">Jan</div>
                  <div className="c-event__day">28</div>
                </div>
              </div>
              <div className="c-event__content">
                <div className="c-event__contentWrap">
                  <div className="c-event__title">Insight Exchange Network</div>
                  <p>Join us for this conference showcasing innovation.</p>
                </div>
                <div className="o-button__wrap">
                  <div className="o-button">Get More Insight</div>
                </div>
              </div>
              <div className="c-event__location">Chicago, IL</div>
            </div>
            <div className="c-event__card">
              <div className="c-event__date">
                <div className="c-event__dateWrap">
                  <div className="c-event__month">Feb</div>
                  <div className="c-event__day">12</div>
                </div>
              </div>
              <div className="c-event__content">
                <div className="c-event__contentWrap">
                  <div className="c-event__title">Citywide Buyer’s Retreat</div>
                  <p>
                    Find out how banks are responding to the changing future of
                    interest...
                  </p>
                </div>
                <div className="o-button__wrap">
                  <div className="o-button">Get More Insight</div>
                </div>
              </div>
              <div className="c-event__location">The Wagner, New York</div>
            </div>
            <div className="c-event__card">
              <div className="c-event__date">
                <div className="c-event__dateWrap">
                  <div className="c-event__month">May</div>
                  <div className="c-event__day">6</div>
                </div>
              </div>
              <div className="c-event__content">
                <div className="c-event__contentWrap">
                  <div className="c-event__title">Research Exchange</div>
                  <p>
                    Find the best online resources to help with your
                    investments...
                  </p>
                </div>
                <div className="o-button__wrap">
                  <div className="o-button">Get More Insight</div>
                </div>
              </div>
              <div className="c-event__location">London, England</div>
            </div>
          </Slider>
          <div className="c-event__logo">
            <img src={logoFooter} alt="" />
          </div>
        </div>
      </div>
    </div>
  );
}
