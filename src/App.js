import "./scss/main.scss";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import Header from "./layout/Header";
import Footer from "./layout/Footer";
import Home from "./page/Home";

function App() {
  return (
    <div className="c-app">
      <Header />
      <Home />
      <Footer />
    </div>
  );
}

export default App;
